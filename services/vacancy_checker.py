from typing import Callable

from logging import Logger


VacancyParser = Callable[[], list[str]]
VacancySender = Callable[[list[str]], None]


class VacancyChecker:
    def __init__(self, vacancy_parser: VacancyParser, vacancy_sender: VacancySender, logger: Logger):
        self.vacancy_parser = vacancy_parser
        self.vacancy_sender = vacancy_sender
        self.logger = logger

    def __call__(self, *args, **kwargs):
        self.logger.info('Checking vacancies has begun')
        vacancies_list = self.vacancy_parser()
        self.vacancy_sender(vacancies_list)
        self.logger.info('Checking vacancies completed')
