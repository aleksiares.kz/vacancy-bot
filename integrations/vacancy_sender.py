from logging import Logger
from telebot import TeleBot


class VacancySender:
    def __init__(self, token: str, user_id: int, logger: Logger):
        self.user_id = user_id
        self.telegram_bot = TeleBot(token=token)
        self.logger = logger

    def __call__(self, vacancies_list: list[str]) -> None:
        self.logger.info('Started sending vacancies in telegram')
        for vacancy in vacancies_list:
            self.send_vacancy(vacancy=vacancy)
        self.logger.info('Completed sending vacancies in telegram')

    def send_vacancy(self, vacancy: str) -> None:
        message_text = self.generate_message_text(vacancy_text=vacancy)
        self.telegram_bot.send_message(chat_id=self.user_id, text=message_text)

    @staticmethod
    def generate_message_text(vacancy_text: str) -> str:
        return f'Найдена вакансия: {vacancy_text}'
