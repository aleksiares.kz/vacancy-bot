import requests
from logging import Logger
from bs4 import BeautifulSoup


class VacancyParser:
    def __init__(self, url: str, pattern: str, logger: Logger):
        self.url = url
        self.pattern = pattern
        self.logger = logger

    def __call__(self, *args, **kwargs) -> list[str]:
        self.logger.info('Parsing of the vacancies page has begun')
        html_page = requests.get(self.url)
        parser = BeautifulSoup(html_page.content, 'html.parser')
        parsed_text = parser.get_text()
        vacancies_list = self._find_sentences_in_text_by_pattern(text=parsed_text, pattern=self.pattern)
        self.logger.info(f'Found {len(vacancies_list)} vacancies by pattern "{self.pattern}"')
        return vacancies_list

    @staticmethod
    def _break_text_into_sentences(text: str) -> list[str]:
        lines_list = text.split('\n')
        lines_list = [line.lstrip() for line in lines_list]
        lines_list = [line.rstrip() for line in lines_list]
        return lines_list

    def _find_sentences_in_text_by_pattern(self, text: str, pattern) -> list[str]:
        result_lines = []
        for line in self._break_text_into_sentences(text):
            if pattern in line:
                result_lines.append(line)
        return result_lines
