import sys

from loguru import logger

LOGGER_FORMAT = '<green>{time:YYYY.MM.DD HH:mm:ss }</green> | <cyan>{level} | {module}</cyan>: {message}'
LOG_FILE = "../logs.log"
logger.remove()
logger.add(
    sink=sys.stderr,
    format=LOGGER_FORMAT,
    level='INFO',
)
logger.add(
    sink=LOG_FILE,
    format=LOGGER_FORMAT,
    level='INFO',
)

# In seconds
VACANCY_CHECK_PERIOD = 60
URL_FOR_PARSING = 'https://vkomandu.ufanet.ru/vacancy/ufa'
PARSING_PATTERN = 'Разработчик'
