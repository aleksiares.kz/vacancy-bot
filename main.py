from fastapi import FastAPI
from fastapi_utils.tasks import repeat_every

from settings import (
    VACANCY_CHECK_PERIOD,
    URL_FOR_PARSING,
    PARSING_PATTERN,
    TELEGRAM_TOKEN,
    TELEGRAM_USER_ID,
)
from settings import logger
from services.vacancy_checker import VacancyChecker
from integrations.vacancy_parser import VacancyParser
from integrations.vacancy_sender import VacancySender

app = FastAPI()


@app.on_event('startup')
@repeat_every(seconds=VACANCY_CHECK_PERIOD)
def check_vacancy():
    vacancy_parser = VacancyParser(url=URL_FOR_PARSING, pattern=PARSING_PATTERN, logger=logger)
    vacancy_sender = VacancySender(token=TELEGRAM_TOKEN, user_id=TELEGRAM_USER_ID, logger=logger)
    VacancyChecker(vacancy_parser=vacancy_parser, vacancy_sender=vacancy_sender, logger=logger)()
